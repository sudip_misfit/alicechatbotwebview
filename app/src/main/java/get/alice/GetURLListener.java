package get.alice;

import get.ResponseBody;

public interface GetURLListener extends BaseApiCallListener{
    void onSuccess(ResponseBody responseBody);
    void onFailed(String message, int responseCode);
}

