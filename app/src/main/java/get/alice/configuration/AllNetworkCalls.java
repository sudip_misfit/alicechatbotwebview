package get.alice.configuration;

import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface AllNetworkCalls {

    @GET("{url}")
    Call<ResponseBody> getRequest(
            @Path(value = "url", encoded = true) String path,
            @QueryMap Map<String, String> hashMap
    );

    @FormUrlEncoded
    @POST("{url}")
    Call<ResponseBody> postRequest(
            @Path(value = "url", encoded = true) String path,
            @FieldMap Map<String, String> hashMap
    );

    @GET("{url}")
    Call<ResponseBody> getRequest(
            @Path(value = "url", encoded = true) String path
    );

    @Headers("Content-Type: application/json")
    @POST("{url}")
    Call<ResponseBody> postRequest(
            @Path(value = "url", encoded = true) String path,
            @Body String body
    );

    @Headers("Content-Type: application/json")
    @POST("{url}")
    Call<ResponseBody> postJsonObject(
            @Path(value = "url", encoded = true) String path,
            @Body JsonObject body
    );

    @FormUrlEncoded
    @PUT("{url}")
    Call<ResponseBody> putRequest(
            @Path(value = "url", encoded = true) String path,
            @FieldMap Map<String, String> hashMap
    );

    @Multipart
    @PUT("{url}")
    Call<ResponseBody> putRequestWithFile(
            @Path(value = "url", encoded = true) String path,
            @PartMap Map<String, RequestBody> hashMap
    );

    @Multipart
    @POST("{url}")
    Call<ResponseBody> sendDocuments(
            @Path(value = "url", encoded = true) String path,
            @PartMap Map<String, RequestBody> partMap
    );

    @Multipart
    @POST("{url}")
    Call<ResponseBody> sendDataWithDocuments(
            @Path(value = "url", encoded = true) String path,
            @PartMap Map<String, RequestBody> partMap
    );

    @DELETE("{url}")
    Call<ResponseBody> deleteRequest(
            @Path(value = "url", encoded = true) String path
    );

    @Multipart
    @PATCH("{url}")
    Call<ResponseBody> patchRequestWithFile(
            @Path(value = "url", encoded = true) String path,
            @PartMap Map<String, RequestBody> partMap
    );

    @FormUrlEncoded
    @PATCH("{url}")
    Call<ResponseBody> patchRequest(
            @Path(value = "url", encoded = true) String path,
            @FieldMap Map<String, String> hashMap
    );

}
