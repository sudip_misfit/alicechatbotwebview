package get.alice;

public interface BaseApiCallListener {
    void startLoading(String requestId);
    void endLoading(String requestId);
}
