package get.alice;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Random;

import get.alice.configuration.ApiHandler;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static get.alice.configuration.ResponseCode.INVALID_JSON_RESPONSE;


public class ApiManager {

    private Context context;
    ApiHandler apiHandler;

    private String reqIdGetAppUrl;

    GetURLListener getURLListener;


    public ApiManager(Context context) {
        this.context=context;

        apiHandler= new ApiHandler(context) {
            @Override
            public void startApiCall(String requestId) {
                if(requestId.equals(reqIdGetAppUrl)){
                    getURLListener.startLoading(requestId);
                }
            }
            @Override
            public void endApiCall(String requestId) {
                if(requestId.equals(reqIdGetAppUrl)){
                    getURLListener.endLoading(requestId);
                }
            }

            @Override
            public void downloadProgress(String requestId, int progress) {

            }

            @Override
            public void successResponse(String requestId, ResponseBody responseBody, String baseUrl, String path, String requestType) {
                if (requestId.equals(reqIdGetAppUrl)){
                    try {
                        JSONObject jsonObject = new JSONObject(responseBody.string());
                        getURLListener.onSuccess(new Gson().fromJson(jsonObject.toString(), get.ResponseBody.class));
                    } catch (Exception e) {
                        e.printStackTrace();
                        getURLListener.onFailed("Invalid  Response", INVALID_JSON_RESPONSE);
                    }
                }
            }

            @Override
            public void failResponse(String requestId, int responseCode, String message) {
                if(requestId.equals(reqIdGetAppUrl)){
                    getURLListener.onFailed(message, responseCode);
                }
            }
        };
    }

    public String getAppUrl(String platformID, String primaryID, GetURLListener getURLListener){
        this.getURLListener = getURLListener;
        this.reqIdGetAppUrl = System.currentTimeMillis() + ""; // random String
        HashMap<String, String> hashMap= new HashMap<>();
        hashMap.put("platform_id", platformID);
        hashMap.put("primary_id", primaryID);
        apiHandler.httpRequest("https://live-v3.getalice.ai/api/", "bots/get_app_url", "post", reqIdGetAppUrl, hashMap);
        return reqIdGetAppUrl;
    }

}